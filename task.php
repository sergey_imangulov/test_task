<!DOCTYPE html>
<html lang = "ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Форма регистрации</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="style/style.css">
      </head>

      <body>
          <div class="container mt-4">
              <h1>Шаг второй</h1>
          <form  action="task_2.php"  method="post">

            <p><?php echo $_POST['name'];?></p>
            <p><?php echo $_POST['phone'];?></p>

      <button class="btn btn-success"
      type="button" value="Back"
      onclick="history.back(); ">Назад</button>

      <button class="btn btn-success"
      type="submit">Добавить еще доверенное лицо</button>

      <button class="btn btn-success"
      type="submit" >Далее</button>

          </form>

          </div>

      </body>
</html>
