<!DOCTYPE html>
<html lang = "ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Форма регистрации</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="style/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="jquery.maskedinput-master/dist/jquery.maskedinput.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container mt-4">
            <h1>Шаг первый</h1>
            <form  action="task.php"  method="post">

                <input type="text" autocomplete="on" class="form-control" name="name"
                id="name" placeholder="ФИО"><br>

                <input type="text" autocomplete="on" class="mask-phone form-control" name="phone"
                id="phone" placeholder="Номер телефона"><br>

                <input type="text" autocomplete="on" class="form-control" name="address"
                id="address" placeholder="Адрес"><br>

                <button class="btn btn-success" onClick="ValidPhone();"
                 type="submit">Далее</button>

            </form>

        </div>
        <script type='text/javascript' src='/ntsaveforms.js'></script>
          <script>
        $('.mask-phone').mask('+7 (999) 999-99-99');
      </script>
      <script type="text/javascript">

      function ValidPhone() {
    var re = /^[\d]{1}\ \([\d]{2,3}\)\ [\d]{2,3}-[\d]{2,3}-[\d]{2,3}$/;
    var myPhone = document.getElementById('phone').value;
    var valid = re.test(myPhone);
    if (valid) output = 'Номер телефона введен правильно!';
    else output = 'Номер телефона введен неправильно!';
    document.getElementById('message').innerHTML = document.getElementById('message').innerHTML+'<br />'+output;
    return valid;
}
</script>
    </body>
</html>
